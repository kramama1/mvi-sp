import os
import cv2


def mp4_to_jpegs(input, output):

    cap = cv2.VideoCapture(input)

    if not os.path.exists(input):
        raise Exception('Path doesn\'t exist.')
    
    for path in [output, os.path.join(output, 'original'), os.path.join(output, 'downscaled')]:
        if not os.path.exists(path):
            os.makedirs(path)

    print('Starting conversion.')

    currentFrame = 0
    counter = 0

    while(True):
        ret, frame = cap.read()
        if not ret: break

        original_name = os.path.join(output, 'original', str(counter) + '.jpg')
        downscaled_name = os.path.join(output, 'downscaled', str(counter) + '.jpg')
        
        frame = cv2.resize(frame, (224, 224))
        downscaled_frame = cv2.resize(frame, (56, 56))


        cv2.imwrite(original_name, frame)
        cv2.imwrite(downscaled_name, downscaled_frame)

        currentFrame += 1  
        counter += 1

    cap.release()
    cv2.destroyAllWindows()

    print('Done')


if __name__ == "__main__":
    mp4_to_jpegs('./dataset/valid.mp4', './dataset/jpegs/test/')
