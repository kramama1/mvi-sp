# Video resolution upscaling using neural networks
with keras, based on SRGAN architecture

[Google colab notebook](https://colab.research.google.com/drive/134Cax2cI6gPg6IZKiOPNDNFbTeNYWLBN?usp=sharing)

[Final report](https://gitlab.fit.cvut.cz/kramama1/mvi-sp/blob/master/final_report.pdf)

[Output video containing both original, downscaled and generated videos](https://gitlab.fit.cvut.cz/kramama1/mvi-sp/blob/master/video_output.mp4)
